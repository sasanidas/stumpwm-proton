(asdf:defsystem #:stumpwm-proton
  :serial t
  :description ""
  :author "Fermin MF"
  :license "GPLv3"
  :depends-on (:stumpwm :alexandria
	       :cl-ppcre :uiop
	       :bordeaux-threads :dmenu)
  :components ((:file "package")
	       (:file "proton")))

