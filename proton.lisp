(in-package #:stumpwm-proton)

;;TODO Add persistence with a file and dmenu completion

(defstruct software name path args)

(defvar *proton-executable* #P"~/Games/wine")

(defvar *config-file* #P"~/.stumpwm-proton.lisp")

(defvar *config-file-cache* nil)

(defun remove-cache () (setf *config-file-cache* nil))

(defun get-software (config-file)
  (when *config-file-cache*
    (return-from get-software *config-file-cache*))

  (if (uiop:file-exists-p config-file)
      (let* ((raw-file (alexandria:read-file-into-string config-file))
	     (config-list (read-from-string raw-file)))
	(setf *config-file-cache*
	      (loop for (name path args) in config-list
		    collect (make-software :name name
					   :path path
					   :args args))))
      (error "File ~a doesn't exist." config-file)))

(defun launch-software (name)
  (when-let ((software (find name (get-software *config-file*)
			     :test #'string-equal :key #'software-name)))
    (bt:make-thread
     #'(lambda ()
	 (ignore-errors
	  (uiop:run-program
	   (format nil "~a ~a"
		   (namestring *proton-executable*)
		   (namestring (software-path software)))))))))

(stumpwm:defcommand proton-reload () ()
  ""
  (remove-cache)
  (get-software *config-file*))

(stumpwm:defcommand proton-select () ()
  ""
  (let* ((software-list (mapcar #'software-name (get-software *config-file*)))
	 (selection (dmenu::dmenu :item-list software-list
				  :prompt "Run: "
				  :vertical-lines (length software-list))))
    (when selection (launch-software selection))))




